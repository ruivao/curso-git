func printAndCount(string: String) -> Int {
    print(string)
    return string.count
}

func openSource(){
    print("Teste")
}

func process(){
    print("process...")
}

func name(name: String){
    print("José da Silva")
}

func printWithoutCounting(string: String) {
    let _ = printAndCount(string: string)
}

printAndCount(string: "hello, world")
// prints "hello, world" and returns a value of 12
printWithoutCounting(string: "hello, world")
// prints "hello, world" but doesn't return a value